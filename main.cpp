#include <SFML/Graphics.hpp>

/*********************************************
 *
 *      PROYECTO/PLANTILLA SFML
 *
 *
 * Proporciona:
 *      - Firme estructura dividida por funcionalidad
 *      - Control de tiempo y frames
 *      - Movimiento del Sprite
 *		- Utiliza contenedores de recursos genéricos
 *
 *********************************************/

#include "Clases/Game.h"

int main(int, char const**)
{
    Game game;
    game.run();
}
