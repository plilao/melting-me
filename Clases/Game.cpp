#include "Game.h"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window.hpp>


/************ GLOBAL VARIABLES ************/
const sf::Time Game::timePerFrame = sf::seconds(1.f/15.f);
const int width = 640, height = 480;
const float segStatistics = 0.5f; //refreshing statistics period (seconds)




/************ CONSTRUCTOR **************/
Game::Game()
: window(sf::VideoMode(width, height, 24), "TITULO APLICACION", sf::Style::Close)
, contFonts()
, contTextures()
, mStatisticsText()
, player()
, isMovingUp(false)
, isMovingDown(false)
, isMovingRight(false)
, isMovingLeft(false)
, mIsRotatingLeft(false)
, mIsRotatingRight(false)
, firstTime(true)
, isInterpolating(false)
{
	window.setVerticalSyncEnabled(true); //Para evitar cortes en los refrescos
	window.setFramerateLimit(125);	//Establecemos maximo real de procesamiento (aunque trabajamos con 60)
	

   // Load Resources. We use generic containers
	try{
		contTextures.load(Textures::Plane, "Resources/Eagle.png");
		contFonts.load(Fonts::OpenSans, "Resources/OpenSans-Regular.ttf");
	}
	catch (std::runtime_error& e)	{
		std::cout << "Exception: " << e.what() << std::endl;
		exit(0);
	}
    
	
   //Item configuration
	player.Init(contTextures.get(Textures::Plane),200.f, 250.f, 0.0f, 0.0f, 0.0f, 0.0f);
	
	mStatisticsText.setFont(contFonts.get(Fonts::OpenSans));
	mStatisticsText.setPosition(5.f, 5.f);
	mStatisticsText.setCharacterSize(13);
	mStatisticsText.setFillColor(sf::Color::Black);
	mStatisticsText.setString("Interpolacion Desactivada (X)");

	interpolation = 0;
}



/**************  MAIN METHODS **************/

void Game::run()    //Main method
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;  //Time since last frame change
	
	
	while (window.isOpen())
	{
		sf::Time elapsedTime = clock.restart();     //Update variables
		timeSinceLastUpdate += elapsedTime;
        
		processEvents();
		
		
        //Llevamos control en las actualizaciones por frame
		while (timeSinceLastUpdate > timePerFrame)   // 15 times/second
		{
			timeSinceLastUpdate -= timePerFrame;
			update(timePerFrame);       
		}
		
		interpolation = (float)std::min(1.f, timeSinceLastUpdate.asSeconds() / timePerFrame.asSeconds());

        render(interpolation);
	}
}



/***************  PHYSICS AND DISPLAY METHODS *************/

void Game::update(sf::Time elapsedTime)     //Update physics
{
	float horiz = 0.f, vertic=0.f;
	if(!firstTime)
	{
		sf::Vector2f vel;
		
		if(isMovingDown)
			vertic = 300.f;
		if(isMovingUp)
			vertic = -300.f;
		if(isMovingLeft)
			horiz = -300.f;
		if(isMovingRight)
			horiz = 300.f;
			
		vel = sf::Vector2f(horiz,vertic);
		player.Update(vel, elapsedTime);
	}
	firstTime=false;
}

void Game::render(float interpolation)     //DRAW
{
	window.clear(sf::Color::White);
	//window.draw(sprite);
	
	//INVOKE PLAYER'S DRAW METHOD
	if(isInterpolating)
		player.DrawWithInterpolation(window, interpolation);
	else
		player.Draw(window);
	
	window.draw(mStatisticsText);
	window.display();
}





/************** EVENTOS ****************/

void Game::processEvents()   //Captura y procesa eventos
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::KeyPressed:
				handlePlayerInput(event.key.code, true);
				break;
                
			case sf::Event::KeyReleased:
				handlePlayerInput(event.key.code, false);
				break;
                
			case sf::Event::Closed:
				window.close();
				break;

			default: break;
		}
	}
}

// Realiza las operaciones correspondientes a cada Evento
void Game::handlePlayerInput(sf::Keyboard::Key key, bool isPressed)
{
	if (key == sf::Keyboard::Up)            //Traslaciones
		isMovingUp = isPressed;
	else if (key == sf::Keyboard::Down)
		isMovingDown = isPressed;
	else if (key == sf::Keyboard::Left)
		isMovingLeft = isPressed;
	else if (key == sf::Keyboard::Right)
		isMovingRight = isPressed;
    
    else if (key == sf::Keyboard::A)        //Rotaciones
        mIsRotatingLeft = isPressed;
    else if (key == sf::Keyboard::S)
        mIsRotatingRight = isPressed;
	
	else if (key == sf::Keyboard::X && isPressed){
		isInterpolating = !isInterpolating;
		
		if(isInterpolating)
			mStatisticsText.setString("Interpolacion Activada (X)");
		else
			mStatisticsText.setString("Interpolacion Desactivada (X)");
	}
}
